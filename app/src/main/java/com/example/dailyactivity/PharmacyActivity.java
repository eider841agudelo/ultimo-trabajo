package com.example.dailyactivity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.dailyactivity.API.GetApi;
import com.example.dailyactivity.API.Post;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PharmacyActivity extends AppCompatActivity {
    ListView listView;
    Spinner localidad, comuna, region;
    ArrayList<String> dataApi = new ArrayList<>();
    ArrayList<Integer> spRegion = new ArrayList<>();
    HashSet<Integer> regionNoRepeat = new HashSet<>();
    ArrayList<String> spLocalidad = new ArrayList<>();
    HashSet<String> localidadNoRepeat = new HashSet<>();
    ArrayList<String> spComuna = new ArrayList<>();
    HashSet<String> comunaNoRepeat = new HashSet<>();
    ArrayAdapter arrayAdapter = null;
    ArrayAdapter adapterRegion = null;
    ArrayAdapter adapterLocalidad = null;
    ArrayAdapter adapterComuna = null;
    Object dataLocalidad, dataRegion, dataComuna;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pharmacy);

        //Buscador para la Region
        adapterRegion = new ArrayAdapter(this, android.R.layout.simple_spinner_item, spRegion);
        region = findViewById(R.id.spRegion);
        region.setAdapter(adapterRegion);
        region.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener( ) {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dataRegion = region.getSelectedItem();
                //spLocalidad.stream().filter(x->x.equals(recolector.contains(dataRegion))).map(x->spLocalidad);
                //adapterLocalidad.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Buscador para la localidad
        adapterLocalidad = new ArrayAdapter(this, android.R.layout.simple_spinner_item, spLocalidad);
        localidad = findViewById(R.id.spLocalidad);
        localidad.setAdapter(adapterLocalidad);
        localidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener( ) {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dataLocalidad = localidad.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Buscador para la comuna
        adapterComuna = new ArrayAdapter(this, android.R.layout.simple_spinner_item, spComuna);
        comuna = findViewById(R.id.spComuna);
        comuna.setAdapter(adapterComuna);
        comuna.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener( ) {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dataComuna = comuna.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Filtro entre localidad y comuna para mostrar las farmacias
        arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, dataApi);
        listView = findViewById(R.id.listPharmacy);
        listView.setAdapter(arrayAdapter);

        //Se crea el evento clic de cada item del listView
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Se crea su respectivo intent y se especifica la clase a la que va a abrir
                Intent intent = new Intent(PharmacyActivity.this, CallMapActivity.class);

                //Se captura la posicion del clic y se envia el dato con start activity
                intent.putExtra("objectData", dataApi.get(position));
                startActivity(intent);
            }
        });

        getPost();
    }

    private void getPost(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://farmanet.minsal.cl/index.php/ws/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GetApi getApi = retrofit.create(GetApi.class);
        Call<List<Post>> call = getApi.getPost();
        call.enqueue(new Callback<List<Post>>( ) {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if(!response.isSuccessful()){
                    Toast.makeText(PharmacyActivity.this, "Codigo https:"+response.code(), Toast.LENGTH_LONG).show( );
                }

                //Busca la region, localidad, comuna
                for (Post post: response.body()){
                    regionNoRepeat.add(post.getFk_region());
                    localidadNoRepeat.add(post.getLocalidad_nombre());
                    comunaNoRepeat.add(post.getComuna_nombre());
                    dataApi.add(post.getLocal_nombre());
                    post.setLocal_lng(post.getLocal_lng());
                    post.setLocal_lat(post.getLocal_lat());
                    post.setFuncionamiento_dia(post.getFuncionamiento_dia());
                    post.setFuncionamiento_hora_apertura(post.getFuncionamiento_hora_apertura());
                    post.setFuncionamiento_hora_cierre(post.getFuncionamiento_hora_cierre());
                }
                for (Integer region : regionNoRepeat){
                    spRegion.add(region);
                }
                for (String localidad : localidadNoRepeat){
                    spLocalidad.add(localidad);
                }
                for (String comuna : comunaNoRepeat){
                    spComuna.add(comuna);
                }

                regionNoRepeat.clear();
                adapterRegion.notifyDataSetChanged();
                localidadNoRepeat.clear();
                adapterLocalidad.notifyDataSetChanged();
                comunaNoRepeat.clear();
                adapterComuna.notifyDataSetChanged();
                arrayAdapter.notifyDataSetChanged();
            }
            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Toast.makeText(PharmacyActivity.this, "Ha ocurrido un error al intentar traer los datos", Toast.LENGTH_SHORT).show( );
            }
        });
    }

    public void searchButtom(View view) {
        Toast.makeText(this, "Actualmente se encuentra fuera de servicio el filtro. Lamentamos los inconvenientes", Toast.LENGTH_LONG).show( );
    }
}