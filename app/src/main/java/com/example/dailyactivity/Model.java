package com.example.dailyactivity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;


public class Model extends AppCompatActivity {
    public SQLiteDatabase getCon(Context context){
        SQLiteConnect connect = new SQLiteConnect(context,"dbuser", null,1);
        SQLiteDatabase database = connect.getWritableDatabase();
        return database;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_model);

        Button btnHome = findViewById(R.id.btnHome);
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent(Model.this, InsertUsersActivity.class);
                startActivity(intent);
            }
        });
    }

    int insertDataUser(Context context, DataUsersDTO dataUsersDTO){
        int response = 0;
        String sqlQueryInsert = "INSERT INTO dataUsers (name,age, phone, address) VALUES ('"+dataUsersDTO.getName()+"','"+dataUsersDTO.getAge()+"','"+dataUsersDTO.getPhone()+"', '"+dataUsersDTO.getAddress()+"')";
        SQLiteDatabase database = this.getCon(context);
        try {
            database.execSQL(sqlQueryInsert);
            response = 1;
        }catch (Exception err){
            Log.w("Model", "insertUser: catch => " + err );
        }
        database.close();
        return response;
    }
    public ArrayList searchData(Context context) {
        //Se crea el array donde se almacenarán los datos
        ArrayList<String> listData = new ArrayList<>();

        try {
            //Se ejecuta la sentecia sqls
            SQLiteDatabase database = this.getCon(context);
            Cursor sqlQuerySearch = database.rawQuery ("SELECT * FROM dataUsers", null);

            //Se recorre los datos encontrados
            if (sqlQuerySearch.moveToFirst()) {
                do {
                    //Se realiza un recorrido por la filas y se van guardando los datos
                    listData.add(sqlQuerySearch.getString(0) + " | " + sqlQuerySearch.getString(1)+ " | " + sqlQuerySearch.getString(2)+ " | " + sqlQuerySearch.getString(3));
                    //Cuando no hayan mas datos que mostrar, terminara el ciclo
                } while (sqlQuerySearch.moveToNext());
            }
            //Se retornan los datos de la lista y se cierra la consulta

            database.close();
        }catch (Exception err) {
            Log.w("Model", "insertUser: catch => " + err );
        }
        return listData;
    }
}