package com.example.dailyactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {

    ListView listView;
    ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_model);

        //Se instancia el model y asigna a listView su respectivo id de la vista
        listView = (ListView)findViewById(R.id.listViewData);
        Model dbModel = new Model();

        //Se envia el dato hacia el model
        final ArrayList<DataUsersDTO> data = dbModel.searchData(SearchActivity.this);

        //Se crea el adaptador
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, data);
        listView.setAdapter(adapter);
    }

    //evento del boton que envia al layout de guardar datos
    public void InsertData (View view) {
        Intent intent = new Intent(this, InsertUsersActivity.class);
        startActivity(intent);
    }
}