package com.example.dailyactivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    //Se instancian los objetos
    private FirebaseAuth mAuth;
    TextView etEmail, etRepeatEmail, etPassword;
    public MediaPlayer ring;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Se inicializa FireBase
        mAuth = FirebaseAuth.getInstance();

        //Se apunta el objeto ha la vista
        etEmail = findViewById(R.id.etEmail);
        etRepeatEmail = findViewById(R.id.etRepeatEmail);
        etPassword = findViewById(R.id.etPassword);
    }

    //Mira si el usuario ha iniciado sesion
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);

        ring = MediaPlayer.create(MainActivity.this, R.raw.lib);
        ring.start();
    }

    //Crear Cuentas De Usuario
    public void createAccount (String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("TAG", "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Toast.makeText(MainActivity.this, "Se Ha Creado Usuario Correctamente", Toast.LENGTH_SHORT).show();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("TAG", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // ...
                    }
                });
    }

    private void updateUI(FirebaseUser firebaseUser){
        Log.i("info", "updateUI: "+ firebaseUser);
    }

    //Se crea el evento clic para el boton registrarse
    public void SingUPUser(View view){
        //Se capturan los datos en las variables
        String email = etEmail.getText().toString();
        String repeatEmail = etRepeatEmail.getText().toString();
        String password = etPassword.getText().toString();

        //Se valida que los datos tengan algun valor
        if(!email.isEmpty() && !repeatEmail.isEmpty() && !password.isEmpty()) {
            //Se confirma que el email sea identico
            if (email.equals(repeatEmail)){
                //Se llama a la clase de Firebase para crear el usuario
                createAccount(email, password);
                Toast.makeText(this, "Se ha registrado correctamente", Toast.LENGTH_SHORT).show( );
                //Se envia a la siguiente actividad una vez registrado
                Intent intent = new Intent(this, InsertUsersActivity.class);
                startActivity(intent);

            //Por si los correos no coinciden
            } else {
                Toast.makeText(this, "Los correos no coinciden", Toast.LENGTH_SHORT).show( );
            }

        //Por si algun dato es nulo
        } else {
            Toast.makeText(this, "Todos los datos han de ser llenados", Toast.LENGTH_SHORT).show( );
        }
    }

    public void SingInUsers(View view){
        Intent intent = new Intent(this, SingInActivity.class);
        startActivity(intent);
    }
}