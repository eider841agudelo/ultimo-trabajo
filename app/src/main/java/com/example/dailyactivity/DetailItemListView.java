package com.example.dailyactivity;

import android.location.LocationListener;
import android.location.LocationProvider;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class DetailItemListView implements LocationListener {


    CallMapActivity mainActivity;
    TextView tvMessage;

    public CallMapActivity getMainActivity() {
        return mainActivity;
    }

    public void setMainActivity(CallMapActivity mainActivity, TextView tvMessage) {
        this.mainActivity = mainActivity;
        this.tvMessage = tvMessage;
    }


    @Override
    public void onLocationChanged(android.location.Location location) {
        // Este metodo se ejecuta cuando el GPS recibe nuevas coordenadas
        /*String myPosition = "Mi ubicación es: \n"
                + "Latitud = " + location.getLatitude() + "\n"
                + "Longitud = " + location.getLongitude();

        tvMessage.setText(myPosition);*/

        //maps(location.getLatitude(), location.getLongitude());
    }

    public void maps(double lat, double lon) {
        // Fragment del Mapa
        MapActivity fragment = new MapActivity();
        // Enviamos los parametros mediante un bundle
        Bundle bundle = new Bundle();
        bundle.putDouble("lat", new Double(lat));
        bundle.putDouble("lon", new Double(lon));
        fragment.setArguments(bundle);

        //vinculamos el fragment
        FragmentManager fragmentManager = getMainActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.mapView, fragment, null);
        fragmentTransaction.commit();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        switch (status) {
            case LocationProvider.AVAILABLE:
                Log.d("debug", "LocationProvider.AVAILABLE");
                break;
            case LocationProvider.OUT_OF_SERVICE:
                Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                break;
            case LocationProvider.TEMPORARILY_UNAVAILABLE:
                Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                break;
        }
    }

    @Override
    public void onProviderEnabled(String provider) {
        tvMessage.setText("GPS Activado");
    }

    @Override
    public void onProviderDisabled(String provider) {
        tvMessage.setText("GPS Desactivado");
    }
}
