package com.example.dailyactivity;

public class DataUsersDTO {
    private String name;
    private String age;
    private String phone;
    private String address;

    public String getName() {return name;}

    public String getPhone() {return phone;}

    public String getAddress() {return address;}

    public String getAge() {return age;}

    public void setName(String name) {this.name = name;}

    public void setPhone(String phone) {this.phone = phone;}

    public void setAddress(String address) {this.address = address;}

    public void setAge(String age) {this.age = age;}
}
