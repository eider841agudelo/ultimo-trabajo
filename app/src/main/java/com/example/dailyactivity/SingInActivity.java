package com.example.dailyactivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SingInActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    TextView etEmail, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_in);

        mAuth = FirebaseAuth.getInstance();

        //Se apunta el objeto ha la vista
        etEmail = findViewById(R.id.etEmailSignIn);
        etPassword = findViewById(R.id.etPasswordSignIn);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    public void signInWithEmailAndPassword  (String email,String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("TAG", "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);

                            resultSignIn(1);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("TAG", "signInWithEmail:failure", task.getException());
                            updateUI(null);
                            resultSignIn(0);
                        }

                        // ...
                    }
                });
    }

    private void updateUI(FirebaseUser firebaseUser){
        Log.i("info", "updateUI: "+ firebaseUser);
    }
    
    Integer resultSignIn(Integer result){
        if (result == 1) {
            Toast.makeText(this, "Se ha logueado Correctamente", Toast.LENGTH_SHORT).show( );

            //Se envia a la siguiente actividad una vez registrado
            Intent intent = new Intent(this, InsertUsersActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Ha ocurrido un error", Toast.LENGTH_SHORT).show( );
        }
        return result;
    }

    //Se crea el evento clic para el boton registrarse
    public void SignInUser(View view){
        //Se capturan los datos en las variables
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();

        //Se valida que los datos tengan algun valor
        if(!email.isEmpty() && !password.isEmpty()) {
            //Se llama a la clase de Firebase para iniciar sesion
            signInWithEmailAndPassword(email, password);

        //Por si algun dato es nulo
        } else {
            Toast.makeText(this, "Todos los datos han de ser llenados", Toast.LENGTH_SHORT).show( );
        }
    }

    //Se crea el evento del boton para llevar a la actividad de registro
    public void SingUPUsers(View view){
        //Se envia a la siguiente actividad una vez registrado
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}