package com.example.dailyactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

public class InsertUsersActivity extends AppCompatActivity {

    //Se instancian los objetos
    EditText etName, etAddress, etAge, etPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_users);

        //Se localizan los objetos en la vista
        etName = findViewById(R.id.etName);
        etAddress = findViewById(R.id.etAddress);
        etAge = findViewById(R.id.etAge);
        etPhone = findViewById(R.id.etPhone);
    }

    //Si se destruye la app, se cerrará sesion para evitar algun inconveniente
    protected void onDestroy() {
        super.onDestroy();
        FirebaseAuth.getInstance().signOut();
    }

    public void insertData (View view) {
        //Se capturan los datos que tienen los campos de texto
        String name = etName.getText().toString();
        String addres = etAddress.getText().toString();
        String age = etAge.getText().toString();
        String phone =  etPhone.getText().toString();

        //Se verifica que hayan datos en los campos de texto
        if (!name.isEmpty() && !addres.isEmpty() && !age.isEmpty() && !phone.isEmpty()) {
            //Se instancian las clases necesarias
            Model model = new Model();
            DataUsersDTO dataUsersDTO = new DataUsersDTO();

            //Se envian los datos a la clase para capturarlos
            dataUsersDTO.setName(name);
            dataUsersDTO.setAge(age);
            dataUsersDTO.setAddress(addres);
            dataUsersDTO.setPhone(phone);

            //Se envian los datos al modelo para guardarlos en la base de datos
            int result = model.insertDataUser(this, dataUsersDTO);

            //Se valida si se guardan los datos o ha ocurrido algun error
            if  (result!=0){
                Toast.makeText(this, "Se han guardado correctamente los datos", Toast.LENGTH_SHORT).show( );
                clearForm(view);
            } else  {
                Toast.makeText(this, "Ha ocurrido un error al guardar los datos :c", Toast.LENGTH_SHORT).show( );
            }

            //Si no ecisten datos en algun campo de texto
        } else {
            Toast.makeText(this, "Todos los valores deben estar completos antes de guardar los datos", Toast.LENGTH_SHORT).show( );
        }
    }

    public void closeSesion(View view){
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    //Limpia el formulario
    public void clearForm(View view){
        etName.setText("");
        etAddress.setText("");
        etAge.setText("");
        etPhone.setText("");
    }

    public void verDatosGuardados (View view) {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    public void dataApi(View view) {
        Intent intent = new Intent(this,PharmacyActivity.class);
        startActivity(intent);
    }
}