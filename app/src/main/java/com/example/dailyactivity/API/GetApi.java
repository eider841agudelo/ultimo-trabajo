package com.example.dailyactivity.API;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetApi {
    @GET("getLocales")
    Call<List<Post>> getPost();
}
